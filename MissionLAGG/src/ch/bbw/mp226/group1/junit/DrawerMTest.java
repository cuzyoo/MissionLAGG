package ch.bbw.mp226.group1.junit;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.bbw.mp226.group1.engine.drawer.DrawerM;

public class DrawerMTest {
	private DrawerM dm;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.dm = new DrawerM();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testJump() {
		double acc = dm.getAcc();
		dm.jump();
		double newAcc = dm.getAcc();
		if(acc - 4 != newAcc){
			fail("jump funktioniert nicht!");
		}
	}

	@Test
	public void testGenerateNr() {
		int nr1 = dm.generateNr();
		int nr2 = dm.generateNr();
		int nr3 = dm.generateNr();
		
		if(nr1 == nr2 && nr2 == nr3){
			fail("generateNr funktioniert nicht! (Oder unglaublicher Zufall...)");
		}
	}

}
