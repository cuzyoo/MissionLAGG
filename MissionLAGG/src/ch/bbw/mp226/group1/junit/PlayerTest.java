package ch.bbw.mp226.group1.junit;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.bbw.mp226.group1.engine.Player;

public class PlayerTest {
	private Player p;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		p = new Player();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testSetxPos() {
		this.p.setxPos(20);
	}

	@Test
	public void testGetxPos() {
		if(p.getxPos() != 50){
			fail("Fehler beim Getter (Y)");
		}
	}

	@Test
	public void testSetyPos() {
		this.p.setyPos(60);
	}

	@Test
	public void testGetyPos() {
		if(p.getyPos() != 120){
			fail("Fehler beim Getter (Y)");
		}
	}	
	
	@Test
	public void testSetScore() {
		p.setScore(200);
	}

	@Test
	public void testGetScore() {
		if(p.getScore() != 0){
			fail("Fehler beim Getter (Score)");
		}
	}

	

}
