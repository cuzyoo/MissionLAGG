package ch.bbw.mp226.group1.junit;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.bbw.mp226.group1.engine.Tunnel;

public class TunnelTest {
	private Tunnel t;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.t = new Tunnel();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetTopBrick() {
		if(t.getTopBrick().getHeight() != 92){
			fail("Da stimmt was nicht, 'height' sollte 92px hoch sein!");
		}
	}

	@Test
	public void testGetFloorBrick() {
		if(t.getFloorBrick().getHeight() != 92){
			fail("Da stimmt was nicht, 'height' sollte 92px hoch sein!");
		}
	}

}
