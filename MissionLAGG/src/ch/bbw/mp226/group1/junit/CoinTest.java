package ch.bbw.mp226.group1.junit;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.bbw.mp226.group1.objects.Coin;

public class CoinTest {
	private Coin c;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.c = new Coin(13, 37);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetxPos() {
		if(this.c.getxPos() != 13){
			fail("Setter funktioniert nicht");
		}
	}

	@Test
	public void testGetyPos() {
		if(this.c.getyPos() != 37){
			fail("Setter funktioniert nicht");
		}
	}

}
