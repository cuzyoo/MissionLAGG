package ch.bbw.mp226.group1.util.highscore;

import ch.bbw.mp226.group1.Game;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 *
 * @author Alwin
 */
public class HighscoreDrawFrame extends JFrame {

    private static final int Y_OFF = 100;
    private static final int X_OFF_NAME = 70;
    private static final int X_OFF_SCORE = 300;
    private int centerHelper;
    private static final Font TITLE_FONT = new Font("Arial", Font.BOLD, 46);
    private static final Color FONT_COLOR = new Color(234, 71, 92);
    private static final Font TEXT_FONT = new Font("Arial", Font.BOLD, 28);
    private static final Color TEXT_COLOR = new Color(71, 234, 92);
    private static final Color BORDER_COLOR = new Color(10, 180, 10);
    private Dimension size = new Dimension(500, 625);
    private Image bgImage;
    ArrayList<Score> scores;

    public HighscoreDrawFrame() {
        this.scores = HighscoreManager.getInstance().getScores();
        initFrame();
    }

    private void initFrame() {

        try {
            URL url = this.getClass().getResource("/ch/bbw/mp226/group1/util/highscore/bg.jpg");
            bgImage = ImageIO.read(url);
        } catch (IOException ex) {
            Logger.getLogger(HighscoreDrawFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                int code = e.getKeyCode();
                if (code == KeyEvent.VK_ESCAPE) {
                    setVisible(false);
                    Game.removeGame();
                    Game.getInstance().setVisible(true);
                }
            }
        });


        setSize(size);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((int) dim.getWidth() / 2 - getWidth() / 2, (int) dim.getHeight() / 2 - getHeight() / 2);
        setUndecorated(true);
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        centerHelper = getWidth() / 2 - 200;

        g2.drawImage(bgImage, 0, 0, getWidth(), getHeight(), null);

        g2.setStroke(new BasicStroke(8));
        g2.setColor(BORDER_COLOR);
        g2.drawRect(0, 0, getWidth(), getHeight());

        g2.setColor(FONT_COLOR);
        g2.setFont(TITLE_FONT);
        g2.drawString("Highscore", X_OFF_NAME + centerHelper, Y_OFF);

        g2.setFont(TEXT_FONT);
        g2.setColor(TEXT_COLOR);

        int row = 40;

        for (int i = 0; i < 10; i++) {
            g2.drawString("" + (i + 1) + ".", X_OFF_NAME - 60 + centerHelper, Y_OFF + 40 + (i * row));
            try {
                g2.drawString("" + this.scores.get(i).getName(), X_OFF_NAME + centerHelper, Y_OFF + 40 + (i * row));
            } catch (IndexOutOfBoundsException ex) {
                g2.drawString("Nobody ", X_OFF_NAME + centerHelper, Y_OFF + 40 + (i * row));
            }
            try {
                g2.drawString("" + this.scores.get(i).getScore(), X_OFF_SCORE + centerHelper, Y_OFF + 40 + (i * row));
            } catch (IndexOutOfBoundsException ex) {
                g2.drawString("0", X_OFF_SCORE + centerHelper, Y_OFF + 40 + (i * row));
            }
        }
        
        g2.setColor(Color.RED);
        g2.drawString("'Esc' zum Neustart", X_OFF_SCORE -133, (14  * row));
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }
}      