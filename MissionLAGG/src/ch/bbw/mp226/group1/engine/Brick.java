package ch.bbw.mp226.group1.engine;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;

/**
 *
 * @author Levin
 */
public class Brick {

    BufferedImage img;
    private int width;
    private int height;

    public Brick(int x, int y) throws IOException {
        this.width = x;
        this.height = y;
        URL url = this.getClass().getResource("/ch/bbw/mp226/group1/pictures/brick.jpeg");
        img = ImageIO.read(url);
    }

    

    public BufferedImage getImg() {
        return img;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }
}
