package ch.bbw.mp226.group1.engine;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
*
* @author Levin
*/

public class Tunnel{

    private Brick top;
    private Brick floor;
    

    public Tunnel() {
        try {
            top = new Brick(92, 92);
            floor = new Brick(92, 92);
        } catch (Exception ex) {
            Logger.getLogger(Tunnel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public Brick getTopBrick() {
        return top;
    }

    public Brick getFloorBrick() {
        return floor;
    }
    
    
}
