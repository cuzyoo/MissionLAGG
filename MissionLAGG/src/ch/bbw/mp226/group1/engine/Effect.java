/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbw.mp226.group1.engine;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Levin
 */
public class Effect {

    private BufferedImage img;
    private int xPos;
    private int yPos;
    private long created;

    public Effect(int xPos, int yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.created = System.nanoTime();
        
        URL url = url = this.getClass().getResource("/ch/bbw/mp226/group1/pictures/effects/comic_pop.png");
        
        try {
            img = ImageIO.read(url);
        } catch (IOException ex) {
            Logger.getLogger(Effect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the img
     */
    public BufferedImage getImg() {
        return img;
    }

    /**
     * @param img the img to set
     */
    public void setImg(BufferedImage img) {
        this.img = img;
    }

    /**
     * @return the xPos
     */
    public int getxPos() {
        return xPos;
    }

    /**
     * @param xPos the xPos to set
     */
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    /**
     * @return the yPos
     */
    public int getyPos() {
        return yPos;
    }

    /**
     * @param yPos the yPos to set
     */
    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    /**
     * @return the created
     */
    public long getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(long created) {
        this.created = created;
    }
}
