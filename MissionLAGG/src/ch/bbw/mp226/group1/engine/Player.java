package ch.bbw.mp226.group1.engine;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import ch.bbw.mp226.group1.engine.drawer.DrawerM;

/**
*
* @author Levin
*/

public class Player {
    private BufferedImage img;
    private int score = 0;
    private int imgCount = 1;
    private int xPos = 50;
    private int yPos = 120;

    public Player() {

        try {
            URL url = this.getClass().getResource("/ch/bbw/mp226/group1/pictures/player/player1.gif");
            img = ImageIO.read(url);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public BufferedImage getImg() {
        if(imgCount+1 <=11){
            this.imgCount++;
        }else{
            this.imgCount = 1;
        }
        
        try {
            URL url = this.getClass().getResource("/ch/bbw/mp226/group1/pictures/player/player" + imgCount + ".gif");
            img = ImageIO.read(url);
        } catch (IOException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
        return img;
    }

    /**
     * @param img the img to set
     */
    public void setImg(BufferedImage img) {
        this.img = img;
    }

    /**
     * @return the xPos
     */
    public int getxPos() {
        return xPos;
    }

    /**
     * @param xPos the xPos to set
     */
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    /**
     * @return the yPos
     */
    public int getyPos() {
        return yPos;
    }

    /**
     * @param yPos the yPos to set
     */
    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    
    
}