package ch.bbw.mp226.group1.engine.drawer;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import ch.bbw.mp226.group1.engine.Effect;
import ch.bbw.mp226.group1.engine.Player;
import ch.bbw.mp226.group1.engine.Tunnel;
import ch.bbw.mp226.group1.objects.Coin;
import ch.bbw.mp226.group1.objects.Obstacle;

/**
*
* @author Levin
*/

public class DrawerM {
	private DrawerC dc;
	private DrawerV dv;
	
	private Player player;
	private Tunnel tunnel;
	private BufferedImage background;

	private ArrayList<Effect> effects;
	private ArrayList<Coin> coins;
	private ArrayList<Obstacle> obstacles;
    
	private Random rand;
    
	private Font font;
    
	private double acc = 5;
	private double velocity = 0.2;
    
    public DrawerM() {
    	this.dv = new DrawerV(this);
    	this.dc = new DrawerC(this, this.dv);
    	
    	this.dv.addKeyListener(dc);
    	
    	player = new Player();
        tunnel = new Tunnel();
        effects = new ArrayList<>();
        coins = new ArrayList<>();
        obstacles = new ArrayList<>();
        
        rand = new Random();
        
        try {
            URL url = this.getClass().getResource("/ch/bbw/mp226/group1/pictures/background.png");
            background = ImageIO.read(url);
        } catch (IOException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }


        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        try {
        	this.font = Font.createFont(Font.TRUETYPE_FONT, DrawerM.class.getResourceAsStream("/ch/bbw/mp226/group1/util/font.ttf"));
            //this.font = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(this.getClass().getResource("/ch/bbw/mp226/group1/util/font.ttf").getFile()));
            this.font = font.deriveFont(40.0f);
        } catch (FontFormatException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
	}
    
    public void jump() {
        this.acc -= 4;
    }
    
    public void update() {
        if (this.player.getyPos() > this.tunnel.getTopBrick().getHeight()) {
            if (this.player.getyPos() < this.dv.getHeight() - 2 * this.tunnel.getFloorBrick().getHeight() - 15) {
                acc += velocity;
            } else {
                acc = -2;
                this.player.setyPos(this.dv.getHeight() - 2 * this.tunnel.getFloorBrick().getHeight() - 15);
            }
            this.player.setyPos((int) Math.round(this.player.getyPos() + acc));
        } else {
            acc = 2;
            this.player.setyPos(this.tunnel.getTopBrick().getHeight() + 2);
        }

        this.player.setxPos((int) Math.round(this.player.getxPos() + 1));
    }
    
    public int generateNr(){
    	int max = dv.getHeight() - this.tunnel.getFloorBrick().getHeight() - 60;
    	max = Math.abs(max);
        int min = this.tunnel.getFloorBrick().getHeight() + 30;
        System.out.println("Max: " + max + "  Min: " + min);
        int randNr = rand.nextInt((max - min) + 1) + min;
        System.out.println(randNr);
        return randNr;
    }
    
    public int calcAbstand(){
    	return this.player.getxPos() % this.tunnel.getTopBrick().getWidth();
    }
    
    public int calcRest(){
    	return this.player.getxPos() % this.background.getWidth();
    }
    
    public ArrayList<Obstacle> getObstacles() {
		return obstacles;
	}
    
    public Player getPlayer() {
		return player;
	}

	public DrawerC getDc() {
		return dc;
	}

	public DrawerV getDv() {
		return dv;
	}

	public Tunnel getTunnel() {
		return tunnel;
	}

	public BufferedImage getBackground() {
		return background;
	}

	public ArrayList<Effect> getEffects() {
		return effects;
	}

	public ArrayList<Coin> getCoins() {
		return coins;
	}

	public Font getFont() {
		return font;
	}

	public double getAcc() {
		return acc;
	}

	public double getVelocity() {
		return velocity;
	}

	public void setDc(DrawerC dc) {
		this.dc = dc;
	}

	public void setDv(DrawerV dv) {
		this.dv = dv;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void setTunnel(Tunnel tunnel) {
		this.tunnel = tunnel;
	}

	public void setBackground(BufferedImage background) {
		this.background = background;
	}

	public void setEffects(ArrayList<Effect> effects) {
		this.effects = effects;
	}

	public void setCoins(ArrayList<Coin> coins) {
		this.coins = coins;
	}

	public void setObstacles(ArrayList<Obstacle> obstacles) {
		this.obstacles = obstacles;
	}

	public void setFont(Font font) {
		this.font = font;
	}

	public void setAcc(double acc) {
		this.acc = acc;
	}

	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}
    
    
}
