package ch.bbw.mp226.group1.engine.drawer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import ch.bbw.mp226.group1.Game;

/**
*
* @author Levin
*/

public class DrawerC implements KeyListener{
    boolean pressed = false;
    
    private DrawerM dm;
	private DrawerV dv;
	
	public DrawerC(DrawerM dm, DrawerV dv) {
		this.dm = dm;
		this.dv = dv;
	}
    
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    	System.out.println("JUMP");
        if(e.getKeyCode() == 32 && !this.pressed){
            this.pressed = true;
            dm.jump();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(this.pressed){
            this.pressed = false;
        }
    }
    
}
