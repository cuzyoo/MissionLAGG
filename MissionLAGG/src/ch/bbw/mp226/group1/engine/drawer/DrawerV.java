package ch.bbw.mp226.group1.engine.drawer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import ch.bbw.mp226.group1.Game;
import ch.bbw.mp226.group1.engine.Effect;
import ch.bbw.mp226.group1.objects.Coin;
import ch.bbw.mp226.group1.objects.Obstacle;
import ch.bbw.mp226.group1.util.highscore.HighscoreDrawFrame;
import ch.bbw.mp226.group1.util.highscore.HighscoreManager;

/**
*
* @author Levin
*/

public class DrawerV extends JComponent{
	private DrawerM dm;

	public DrawerV(DrawerM dm) {
		this.dm = dm;
	}
	
	@Override
    public void paint(Graphics g) {
        if (Game.getInstance().isRunning()) {
            Rectangle r_player = new Rectangle(dm.getPlayer().getxPos() + 40, dm.getPlayer().getyPos(), 50, 80);
            
            for (Obstacle o : dm.getObstacles()) {
                Rectangle r_obstacle = new Rectangle(o.getxPos(), o.getyPos(), 50, 50);
                
                if (r_player.intersects(r_obstacle)) { //ENDE
                    Game.getInstance().setVisible(false);
                    g.dispose();

                    String eingabe = JOptionPane.showInputDialog(null, "Geben Sie Ihren Namen ein", "Highscore", JOptionPane.PLAIN_MESSAGE);
                    HighscoreManager.getInstance().addScore(eingabe, dm.getPlayer().getScore());

                    new HighscoreDrawFrame().setVisible(true);

                }
            }


            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON); 
            g.translate(-1 * dm.getPlayer().getxPos(), 0);

            for (int i = 0; i < 3; i++) {
                g.drawImage(dm.getBackground(), i * dm.getBackground().getWidth() + dm.getPlayer().getxPos() - dm.calcRest(), 0, this);
            }

            //Player
            g.drawImage(dm.getPlayer().getImg(), dm.getPlayer().getxPos() + 10, dm.getPlayer().getyPos(), 150, 100, null);

            if (dm.calcAbstand() == 0) { //Neuer coin
            	
                int randomNum = dm.generateNr();
                
                ArrayList<Coin> c = dm.getCoins();
                c.add(new Coin(dm.getPlayer().getxPos() + 1200 , randomNum));
                dm.setCoins(c);

                randomNum = dm.generateNr();
                
                ArrayList<Obstacle> o = dm.getObstacles();
                o.add(new Obstacle(dm.getPlayer().getxPos() + 1200, randomNum));
                
                dm.setObstacles(o);
            }

            for (Obstacle o : dm.getObstacles()) {
                g.drawImage(o.getImg(), o.getxPos(), o.getyPos(), 50, 50, this);
                //g.setColor(Color.YELLOW);
                //g.drawRect(o.getxPos(), o.getyPos(), 50, 50);
            }

            //g.drawRect(player.getxPos() + 40, player.getyPos(), 50 , 80 );

            for (int i = 0; i < dm.getCoins().size(); i++) {
                Coin c = dm.getCoins().get(i);
                g.drawImage(c.getImg(), c.getxPos(), c.getyPos(), 50, 50, this);
                Rectangle r_coin = new Rectangle(c.getxPos(), c.getyPos(), 50, 50);
                
                if(r_player.intersects(r_coin)){
                	ArrayList<Coin> coins = dm.getCoins();
                	coins.remove(c);
                	dm.setCoins(coins);
                    dm.getPlayer().setScore(dm.getPlayer().getScore() + 1); 
                }
            }

            //Walls
            for (int i = 0; i < 11; i++) {
                g.drawImage(dm.getTunnel().getTopBrick().getImg(), i * dm.getTunnel().getTopBrick().getWidth() + dm.getPlayer().getxPos() - dm.calcAbstand(), 0, dm.getTunnel().getTopBrick().getWidth(), dm.getTunnel().getTopBrick().getWidth(), null);
                g.drawImage(dm.getTunnel().getFloorBrick().getImg(), i * dm.getTunnel().getFloorBrick().getWidth() + dm.getPlayer().getxPos() - dm.calcAbstand(), this.getHeight() - dm.getTunnel().getFloorBrick().getHeight(), dm.getTunnel().getFloorBrick().getWidth(), dm.getTunnel().getFloorBrick().getWidth(), null);
            }

            if (dm.getPlayer().getyPos() - 10 < dm.getTunnel().getTopBrick().getHeight()) {
                long difNow = 600000001;
                try {
                    difNow = System.nanoTime() - dm.getEffects().get(0).getCreated();
                } catch (IndexOutOfBoundsException ex) {
                }

                if (difNow > 600000000) {
                	ArrayList<Effect> efs = dm.getEffects();
                	efs.add(new Effect(dm.getPlayer().getxPos(), dm.getPlayer().getyPos()));
                }

            }

            for (int i = 0; i < this.dm.getEffects().size(); i++) {
                long difNow = System.nanoTime() - dm.getEffects().get(i).getCreated();


                if (difNow > 600000000) {
                	ArrayList<Effect> eft = dm.getEffects();
                	eft.remove(i);
                	dm.setEffects(eft);
                } else {
                    g.drawImage(dm.getEffects().get(i).getImg(), dm.getEffects().get(i).getxPos(), dm.getEffects().get(i).getyPos() - 60, this);
                }
            }

            g.setColor(Color.YELLOW);

            g.setFont(dm.getFont());
            g.drawString("Score: " + dm.getPlayer().getScore(), dm.getPlayer().getxPos() + 5, 50);

            g.setColor(Color.LIGHT_GRAY);
            g.drawString("Mission LAGG", 400, 550);
        } else {
            Game.getInstance().setVisible(false);
        }
    }
}
