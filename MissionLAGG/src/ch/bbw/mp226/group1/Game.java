package ch.bbw.mp226.group1;

import java.awt.BorderLayout;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;

import ch.bbw.mp226.group1.engine.drawer.DrawerM;

public class Game extends JFrame{
	
	DrawerM drawer;

	public static void main(String[] args) {
		Game.getInstance();
	}
	
	private static Game instance = null;
    private boolean running = true;
    
    public void setRunning(boolean running) {
        this.running = running;
    }

    public boolean isRunning() {
        return running;
    }
    
    public static Game getInstance() {
        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }
    
    public static void removeGame(){
        instance = null;
    }
    
    private Game() {
    	JPanel contentPane = new JPanel(new BorderLayout());
    	this.drawer = new DrawerM();
    	
    	contentPane.add(this.drawer.getDv(), BorderLayout.CENTER);
    	
    	drawer.getDv().setFocusable(true);
    	
    	contentPane.addKeyListener(drawer.getDc());
    	
    	this.add(contentPane);
    	this.setTitle("Mission L|A|G|G");
    	this.setVisible(true);
    	this.setSize(900, 600);
        this.setResizable(false);

        runTimer();
        
    }
    
    public void runTimer() {
        TimerTask action = new TimerTask() {

            public void run() {
            	drawer.getDv().repaint();
            	drawer.update();
            }
        };
        
        Timer caretaker = new Timer();
        caretaker.schedule(action, 1, 15);
    }

}
