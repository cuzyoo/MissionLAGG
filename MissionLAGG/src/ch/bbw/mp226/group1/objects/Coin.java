package ch.bbw.mp226.group1.objects;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
*
* @author Alwin
*/

public class Coin {
    private BufferedImage img;
    private int xPos;
    private int yPos;

    public Coin(int xPos, int yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
        
        URL url = this.getClass().getResource("/ch/bbw/mp226/group1/pictures/coin.png");
        try {
            this.img = ImageIO.read(url);
        } catch (IOException ex) {
            Logger.getLogger(Coin.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public BufferedImage getImg() {
        return img;
    }

    public int getxPos() {
        return xPos;
    }

    public int getyPos() {
        return yPos;
    }
    
    
}
